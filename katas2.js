//SOMA//
/* Escreva uma função chamada "add" que pega dois argumentos e retorna a soma deles.

Você pode usar operadores incorporados para finalizar a definição.

Por exemplo, chamar add(2, 4) deve retornar um resultado 6. */

function add(a,b) {
return a+b;
}
console.log(add (2,4));

//MULTIPLICAÇÂO//
/* Escreva uma função chamada "multiply" que pega dois argumentos e retorna seu produto.

Você não pode usar operadores ou funções aritméticas incorporadas (como o operador de multiplicação *). Em vez disso, você precisará de um loop for que chama a função "add" criada anteriormente.

Por exemplo, chamar multiply(6, 8) deve retornar um resultado 48.*/

function multiply(a, b) {
  let produtoM = a;
  
  for (let i= 1; i < b; i++) {
    produtoM =add (produtoM,a);
  }

  return produtoM;
}
console.log(multiply (6,8));

//POTENCIAÇÃO//
/* Escreva uma função chamada "power" que pega dois argumentos (x e n) e retorna o resultado de x elevado à potência n.

Você não pode usar operadores ou funções aritméticas incorporadas (como o operador de multiplicação * ou o operador de potência/expoente **). Em vez disso, para escrever esta função, use outras funções que você escreveu em katas anteriores.

Outra palavra usada para potência é "exponenciação". Por exemplo, se nós chamarmos power(2, 8), retornaremos 256 ao multiplicar 2 por ele mesmo 8 vezes.

2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 = 256
Se tivéssemos chamado power(3, 4), teríamos multiplicado 3 por ele mesmo 4 vezes:

3 * 3 * 3 * 3 = 81 */

function power(x,n) {
  let produto = 1;
    for (let i= 0; i<n; i++){
    produto=multiply(produto,x);
  }
  return produto;
  //return Math.pow(x,n);
}
console.log(power(2,8));


/*for(let a = 0; F <=100; F++) {
  if(F%5==0){
    console.log(F)*/

    //FATORIAL//
    /* Escreva uma função chamada "factorial" que pega um único argumento e retorna o fatorial dele.
Você não pode usar operadores ou funções aritméticas incorporadas (como o operador de multiplicação *).<br>
 Em vez disso, para escrever esta função, use outras funções que você escreveu em katas anteriores.
Por exemplo, chamar factorial(4) deve retornar um resultado 24. */

function factorial(a) {
  let produtoF = 0;
  
  for(let fat = 1; a > 1; a = a - 1) {

    fat = multiply(fat, a);
    produtoF = fat;

  }
  
  return produtoF;
}

console.log(factorial(4));

//Fibonacci //
/* Escreva uma função chamada "fibonacci" que pega um argumento n e retorna o enésimo número Fibonacci.

Você não pode usar operadores ou funções aritméticas incorporadas. 
Em vez disso, para escrever esta função,
 use outras funções que você escreveu em katas anteriores 
 (como o operador de multiplicação incorporado *). */

 function fibonacci (termo) {

 let penultimo = 0; ultimo = 1;
 let numero;
 
    if (termo <=2){
      numero = termo-1;
 } 

    else 
    for(var count=3; count<=termo ; count++){
      numero = ultimo + penultimo;
      penultimo = ultimo;
      ultimo = numero;
   }
    return numero;
  }
 console.log(fibonacci(8));

